#include <stdio.h>

int main(){
  int num=0, innerCount=0, outerCount=1;
  printf("Input the number : ");
  scanf("%d", &num);
  while(outerCount<=num){
    innerCount = 1;
    while(innerCount<=num){
      printf("%d * %d = %d \n", outerCount , innerCount , outerCount*innerCount);
      innerCount++;
    }
    printf("\n");
    outerCount++;  
  }
  return 0;
}
