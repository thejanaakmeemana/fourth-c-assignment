#include <stdio.h>

int main(){
  int num=0,divider=2,remainder=0;
  printf("Input number : ");
  scanf("%d" , &num);
  while(divider<num){
    remainder = num%divider;
    if(remainder==0){
      break;
    }else{
      divider++;
    }
  }
  if(divider==num){
    printf("%d is a prime number \n", num);
  }else{
    printf("%d isn't a prime number \n", num);
  }
  
  return 0;
}
