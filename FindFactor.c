#include <stdio.h>

int main(){
  int num=0, divider=1,remainder=0;
  printf("Input the number : ");
  scanf("%d", &num);
  while(divider<=num){
    remainder = num%divider;
    if(remainder==0){
      printf("%d is a factor \n", divider);
      divider++;
    }else{
      divider++;
    }
  }
  return 0;
}
