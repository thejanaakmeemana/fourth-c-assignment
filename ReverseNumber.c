#include <stdio.h>

int main(){
  long num=0;
  int remainder=0;
  printf("Input number : ");
  scanf("%ld" , &num);
  while(num!=0){
    remainder = num%10;
    num/=10;
    printf("%d",remainder);
  }
  printf("\n");
  return 0;
}

